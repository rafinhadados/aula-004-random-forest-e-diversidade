```python
from IPython.core.display import display, HTML
display(HTML("""
<style>
.container { width:76% !important; float:right }
ul { font-size: 18px; }
li { margin: 10px 0; }
</style>"""))
```



<style>
.container { width:76% !important; float:right }
ul { font-size: 18px; }
li { margin: 10px 0; }
</style>


<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>


 - Revisão sobre árvore de decisão
 - Ideia geral da Random Forest
   - Não vou entrar nos mínimos detalhes, vamos ver na prática
 - A Random Forest é melhor que a árvore de decisão?
   - Caso particular de <u>ensemble</u> de modelos
 - Melhor para ORDENAR a base
   
<img src="logo_rafinha.png" style='height: 330px'>


```python
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
```


```python
df = pd.read_csv('2feats.csv')
df
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>goout</th>
      <th>absences</th>
      <th>target</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>4</td>
      <td>6</td>
      <td>0</td>
    </tr>
    <tr>
      <th>1</th>
      <td>3</td>
      <td>4</td>
      <td>0</td>
    </tr>
    <tr>
      <th>2</th>
      <td>2</td>
      <td>10</td>
      <td>0</td>
    </tr>
    <tr>
      <th>3</th>
      <td>2</td>
      <td>2</td>
      <td>0</td>
    </tr>
    <tr>
      <th>4</th>
      <td>2</td>
      <td>4</td>
      <td>0</td>
    </tr>
    <tr>
      <th>...</th>
      <td>...</td>
      <td>...</td>
      <td>...</td>
    </tr>
    <tr>
      <th>390</th>
      <td>4</td>
      <td>11</td>
      <td>1</td>
    </tr>
    <tr>
      <th>391</th>
      <td>5</td>
      <td>3</td>
      <td>1</td>
    </tr>
    <tr>
      <th>392</th>
      <td>3</td>
      <td>3</td>
      <td>0</td>
    </tr>
    <tr>
      <th>393</th>
      <td>1</td>
      <td>0</td>
      <td>1</td>
    </tr>
    <tr>
      <th>394</th>
      <td>3</td>
      <td>5</td>
      <td>0</td>
    </tr>
  </tbody>
</table>
<p>395 rows × 3 columns</p>
</div>



# Divisão em base de treino e base de teste


```python
X = df.drop('target',axis=1)
y = df['target']

from sklearn.model_selection import train_test_split

X_tr, X_ts, y_tr, y_ts = train_test_split(X,y, test_size=0.30, random_state=61658)
```


```python
X
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>goout</th>
      <th>absences</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>4</td>
      <td>6</td>
    </tr>
    <tr>
      <th>1</th>
      <td>3</td>
      <td>4</td>
    </tr>
    <tr>
      <th>2</th>
      <td>2</td>
      <td>10</td>
    </tr>
    <tr>
      <th>3</th>
      <td>2</td>
      <td>2</td>
    </tr>
    <tr>
      <th>4</th>
      <td>2</td>
      <td>4</td>
    </tr>
    <tr>
      <th>...</th>
      <td>...</td>
      <td>...</td>
    </tr>
    <tr>
      <th>390</th>
      <td>4</td>
      <td>11</td>
    </tr>
    <tr>
      <th>391</th>
      <td>5</td>
      <td>3</td>
    </tr>
    <tr>
      <th>392</th>
      <td>3</td>
      <td>3</td>
    </tr>
    <tr>
      <th>393</th>
      <td>1</td>
      <td>0</td>
    </tr>
    <tr>
      <th>394</th>
      <td>3</td>
      <td>5</td>
    </tr>
  </tbody>
</table>
<p>395 rows × 2 columns</p>
</div>




```python
from sklearn.tree import DecisionTreeClassifier

dt = DecisionTreeClassifier(max_depth=2, random_state=61658)
dt.fit(X_tr, y_tr)

from sklearn.tree import plot_tree

plt.figure(figsize=(15,5))
plot_tree(dt, filled=True)
```




    [Text(418.5, 226.5, 'X[0] <= 3.5\ngini = 0.332\nsamples = 276\nvalue = [218, 58]'),
     Text(209.25, 135.9, 'X[1] <= 13.0\ngini = 0.161\nsamples = 181\nvalue = [165, 16]'),
     Text(104.625, 45.29999999999998, 'gini = 0.125\nsamples = 164\nvalue = [153, 11]'),
     Text(313.875, 45.29999999999998, 'gini = 0.415\nsamples = 17\nvalue = [12, 5]'),
     Text(627.75, 135.9, 'X[0] <= 4.5\ngini = 0.493\nsamples = 95\nvalue = [53, 42]'),
     Text(523.125, 45.29999999999998, 'gini = 0.452\nsamples = 55\nvalue = [36, 19]'),
     Text(732.375, 45.29999999999998, 'gini = 0.489\nsamples = 40\nvalue = [17, 23]')]




    
![png](aula_004_rf_files/aula_004_rf_8_1.png)
    



```python
23/40
```




    0.575




```python
from sklearn.ensemble import RandomForestClassifier

rf = RandomForestClassifier(max_depth=2, random_state=61658, n_estimators=2)
rf.fit(X_tr, y_tr)
```




    RandomForestClassifier(bootstrap=True, ccp_alpha=0.0, class_weight=None,
                           criterion='gini', max_depth=2, max_features='auto',
                           max_leaf_nodes=None, max_samples=None,
                           min_impurity_decrease=0.0, min_impurity_split=None,
                           min_samples_leaf=1, min_samples_split=2,
                           min_weight_fraction_leaf=0.0, n_estimators=2,
                           n_jobs=None, oob_score=False, random_state=61658,
                           verbose=0, warm_start=False)




```python
plt.figure(figsize=(15,5))
plot_tree(rf.estimators_[0],filled=True)
plt.figure(figsize=(15,5))
plot_tree(rf.estimators_[1],filled=True)
```




    [Text(418.5, 226.5, 'X[1] <= 12.5\ngini = 0.344\nsamples = 167\nvalue = [215, 61]'),
     Text(209.25, 135.9, 'X[1] <= 3.5\ngini = 0.299\nsamples = 144\nvalue = [201, 45]'),
     Text(104.625, 45.29999999999998, 'gini = 0.249\nsamples = 80\nvalue = [117, 20]'),
     Text(313.875, 45.29999999999998, 'gini = 0.354\nsamples = 64\nvalue = [84, 25]'),
     Text(627.75, 135.9, 'X[0] <= 4.5\ngini = 0.498\nsamples = 23\nvalue = [14, 16]'),
     Text(523.125, 45.29999999999998, 'gini = 0.496\nsamples = 16\nvalue = [12, 10]'),
     Text(732.375, 45.29999999999998, 'gini = 0.375\nsamples = 7\nvalue = [2, 6]')]




    
![png](aula_004_rf_files/aula_004_rf_11_1.png)
    



    
![png](aula_004_rf_files/aula_004_rf_11_2.png)
    



```python
from sklearn.metrics import roc_auc_score

max_depths = [2,3,4,5,6,7,8,9,10]
for max_depth in max_depths:
    dt = DecisionTreeClassifier( max_depth=max_depth, random_state=61658 )
    rf = RandomForestClassifier( n_estimators=700, max_depth=max_depth, random_state=61658 )
    
    dt.fit(X_tr, y_tr)
    rf.fit(X_tr, y_tr)
    
    auc_dt = roc_auc_score(y_ts, dt.predict_proba(X_ts)[:,1])
    auc_rf = roc_auc_score(y_ts, rf.predict_proba(X_ts)[:,1])
    
    print( '{max_depth:2d} - {auc_dt:.3f} - {auc_rf:.3f}'.format(max_depth=max_depth,
                                                                 auc_dt=auc_dt,
                                                                 auc_rf=auc_rf,
                                                                ) )
```

     2 - 0.719 - 0.734
     3 - 0.689 - 0.730
     4 - 0.670 - 0.711
     5 - 0.683 - 0.700
     6 - 0.702 - 0.697
     7 - 0.651 - 0.687
     8 - 0.650 - 0.689
     9 - 0.647 - 0.689
    10 - 0.647 - 0.688



```python
dt = DecisionTreeClassifier(max_depth=2, random_state=61658)
rf = RandomForestClassifier(n_estimators=700, max_depth=2, random_state=61658)

dt.fit(X_tr, y_tr)
rf.fit(X_tr, y_tr)
```




    RandomForestClassifier(bootstrap=True, ccp_alpha=0.0, class_weight=None,
                           criterion='gini', max_depth=2, max_features='auto',
                           max_leaf_nodes=None, max_samples=None,
                           min_impurity_decrease=0.0, min_impurity_split=None,
                           min_samples_leaf=1, min_samples_split=2,
                           min_weight_fraction_leaf=0.0, n_estimators=700,
                           n_jobs=None, oob_score=False, random_state=61658,
                           verbose=0, warm_start=False)




```python
alcool  = y==1
nalcool = y==0
```


```python
import numpy as np

xx,yy = np.meshgrid(
    np.arange(0.8,5.2,0.01),
    np.arange(-3,80,0.5)
)
zz = dt.predict_proba(np.c_[xx.ravel(), yy.ravel()])[:, 1].reshape(xx.shape)

plt.figure(figsize=(15,5))
plt.plot(X['goout'][nalcool]    ,X['absences'][nalcool],'bo',label='Não alcolistas')
plt.plot(X['goout'][ alcool]+.05,X['absences'][ alcool],'ro',label='Alcolistas')
plt.contourf(xx,yy,zz, cmap='RdBu_r', alpha=.6,vmin=0,vmax=1)
plt.colorbar()

plt.xlabel('Saídas')
plt.ylabel('Faltas')
plt.legend()
plt.xlim(0.8,5.2)
plt.ylim(-3,80);
```


    
![png](aula_004_rf_files/aula_004_rf_15_0.png)
    



```python
import numpy as np

xx,yy = np.meshgrid(
    np.arange(0.8,5.2,0.01),
    np.arange(-3,80,0.5)
)
zz = rf.predict_proba(np.c_[xx.ravel(), yy.ravel()])[:, 1].reshape(xx.shape)

plt.figure(figsize=(15,5))
plt.plot(X['goout'][nalcool]    ,X['absences'][nalcool],'bo',label='Não alcolistas')
plt.plot(X['goout'][ alcool]+.05,X['absences'][ alcool],'ro',label='Alcolistas')
plt.pcolormesh(xx,yy,zz, cmap='seismic', alpha=.6,vmin=0,vmax=1)
plt.colorbar()

plt.xlabel('Saídas')
plt.ylabel('Faltas')
plt.legend()
plt.xlim(0.8,5.2)
plt.ylim(-3,80);
```

    <ipython-input-25-e46efe4118e8>:12: MatplotlibDeprecationWarning: shading='flat' when X and Y have the same dimensions as C is deprecated since 3.3.  Either specify the corners of the quadrilaterals with X and Y, or pass shading='auto', 'nearest' or 'gouraud', or set rcParams['pcolor.shading'].  This will become an error two minor releases later.
      plt.pcolormesh(xx,yy,zz, cmap='seismic', alpha=.6,vmin=0,vmax=1)



    
![png](aula_004_rf_files/aula_004_rf_16_1.png)
    



```python
np.unique(dt.predict_proba(X)[:,1])
```




    array([0.06707317, 0.29411765, 0.34545455, 0.575     ])




```python
np.unique(rf.predict_proba(X)[:,1])
```




    array([0.08849682, 0.08925165, 0.09120446, 0.09657374, 0.09732857,
           0.09916579, 0.09928138, 0.09942904, 0.10138185, 0.10656545,
           0.10696462, 0.10724271, 0.11464238, 0.11504155, 0.1170582 ,
           0.11790816, 0.12072208, 0.12229061, 0.12347569, 0.12513512,
           0.12879901, 0.13036753, 0.13155261, 0.132468  , 0.19320232,
           0.19352412, 0.19419316, 0.19629362, 0.1972391 , 0.25174328,
           0.25212717, 0.255413  , 0.25555438, 0.25711252, 0.2687464 ,
           0.27055636, 0.27101413, 0.27131846, 0.27145984, 0.27293309,
           0.27702644, 0.28027586, 0.28850122, 0.29102419, 0.29371096,
           0.29518033, 0.29605082, 0.3036126 , 0.3051691 , 0.30781116,
           0.30891366, 0.30915113, 0.31354548, 0.32190942, 0.36382451,
           0.37586767, 0.37651287, 0.38895441, 0.41138946, 0.41750865,
           0.43887194, 0.43985058, 0.44110186, 0.45522083, 0.45538223,
           0.45725444, 0.46158159, 0.46410977, 0.46788602, 0.46799619,
           0.468981  , 0.47011098, 0.4742627 , 0.47442898, 0.50570698,
           0.54701198, 0.57135877, 0.5799243 , 0.58379059, 0.58673682,
           0.59742732])




```python

```
